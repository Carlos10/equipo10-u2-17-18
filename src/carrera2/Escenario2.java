package carrera2;

public class Escenario2{

    public Vista2 vg2;
    public PuntoLlegada2 llegada2;
    public Seleccion2 sel2;
    
    public Escenario2(){
        this.vg2 = new Vista2();
        this.llegada2 = new PuntoLlegada2(1);
        crearHilos2();

    }
    
   
    public void crearHilos2(){
        double ran;
        int valorRan;
        
        ran = Math.random()*10;
        valorRan = (int)ran;
        valorRan = ((valorRan<1 || valorRan>10) ? 5 : valorRan);
        Corredor2 g12 = new Corredor2(vg2, llegada2, sel2);
        g12.setPriority(valorRan);
        g12.setName("Corredor 1");
//        System.out.println("prioridad Galgo1:"+g1.getPriority());       
        
        ran = Math.random()*10;
        valorRan = (int)ran;
        valorRan = ((valorRan<1 || valorRan>10) ? 5 : valorRan);
        Corredor2 g22 = new Corredor2(vg2, llegada2, sel2);
        g22.setPriority(valorRan);
        g22.setName("Corredor 2");
//        System.out.println("prioridad Corredor2:"+g2.getPriority());     
        
        ran = Math.random()*10;
        valorRan = (int)ran;
        valorRan = ((valorRan<1 || valorRan>10) ? 5 : valorRan);
        Corredor2 g32 = new Corredor2(vg2, llegada2, sel2);
        g32.setPriority(valorRan);
        g32.setName("Corredor 3");
//        System.out.println("prioridad Galgo3:"+g3.getPriority());
        
        g12.start();
        g22.start();
        g32.start();
        
    }
       
}