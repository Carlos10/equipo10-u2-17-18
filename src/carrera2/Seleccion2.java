/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrera2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author FVPP
 */
public class Seleccion2 extends JFrame implements ActionListener{
    public JFrame f;
    public JPanel panelMayor, panelNorte, panelCentro, panelSur, panelEste, panelOeste;
    public JLabel lbl5;
    public JButton bs1, bs2;
    public JTextField tf12;
    
    public static String vuelta2;
    
    public Escenario2 gal2;
       
    public Seleccion2(){
    JFrame f = new JFrame();
   
    //Declaración de panel
    panelMayor = new JPanel(new BorderLayout());  
    panelNorte = new JPanel();
    panelCentro = new JPanel();
    panelSur = new JPanel();
    panelEste = new JPanel();
    panelOeste = new JPanel();
    
    //Declaraciones
    FlowLayout fl1 = new FlowLayout (FlowLayout.CENTER);
    panelNorte.setLayout(fl1);
    
    GridBagLayout gbl1 = new GridBagLayout();
    GridBagConstraints gbc1 = new GridBagConstraints();
    panelCentro.setLayout(gbl1);
    
    FlowLayout fl2 = new FlowLayout();
    panelSur.setLayout(fl2);
    
    GridLayout gl2 = new GridLayout(3, 0);
    panelEste.setLayout(gl2);
    
    GridLayout gl3 = new GridLayout(3, 0);
    panelOeste.setLayout(gl3);
    
    
    //Asignación de formatos 
    
    lbl5 = new JLabel ("Cual es su eleccion?: ");
    
    /////////////////////////////////////////////////////////////////
    
    tf12 = new JTextField(25);
    
    //////////////////////////////////////////////////////////////////
    
    bs1 = new JButton("Comenzar");
    bs1.addActionListener(this);
    
    /////////////////////////////////////////////////////////////////
    
    bs2 = new JButton("Cancelar");
    bs2.addActionListener(this);
   //Orientando los objetos
    
    gbc1.anchor = GridBagConstraints.WEST;
    
    gbc1.gridx = 0;
    gbc1.gridy = 0;
    
    panelCentro.add(lbl5,gbc1);
    gbc1.gridy = 0;
    gbc1.gridwidth = 5;
    gbc1.gridx = 1;

    panelCentro.add(tf12,gbc1);
    gbc1.gridy = 1;
    gbc1.gridwidth = 1;
     
      
    panelSur.add(bs1);
    panelSur.add(bs2);
   
    panelMayor.add(panelNorte, BorderLayout.NORTH);
    panelMayor.add(panelCentro, BorderLayout.CENTER);
    panelMayor.add(panelSur, BorderLayout.SOUTH);
    panelMayor.add(panelEste, BorderLayout.EAST);
    panelMayor.add(panelOeste, BorderLayout.WEST);
    
    this.add(panelMayor);
    
    
    this.setTitle("Carreras nivel 2 (tres corredores)");
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setSize(480,200);
    this.setLocationRelativeTo(null);
    this.setVisible(true);
    
    }
    
    public void actionPerformed (ActionEvent evento){
        
        //boton 1
        if (bs1 == evento.getSource()){
            
        vuelta2=tf12.getText();
        
         switch(vuelta2){
            
            case "":
                JOptionPane.showMessageDialog(null, "No ingreso ningun dato!");
                break;
            case "1":
                this.setVisible(false);
                gal2 = new Escenario2();
                break;
            case "2":
                this.setVisible(false);
                gal2 = new Escenario2();
                break;
            case "3":
                this.setVisible(false);
                gal2 = new Escenario2();
                break;
            default:
                JOptionPane.showMessageDialog(null, "Corredor no disponible!");
                tf12.setText("");
                break;
                      
        }
             
        }
 
        //boton 2
        if(bs2 == evento.getSource()){
            System.exit(0);
        }
    }
    
}