/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carrera2;

public class PuntoLlegada2 {
    public int llegada2;

    public PuntoLlegada2() {
    }

    public PuntoLlegada2(int llegada2) {
        this.llegada2 = llegada2;
    }

    public int getLlegada2() {
        return llegada2;
    }

    public void setLlegada2(int llegada2) {
        this.llegada2 = llegada2;
    }

}