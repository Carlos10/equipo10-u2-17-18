package carrera1;

public class Escenario{

    public Vista vg;
    public PuntoLlegada llegada;
    public Seleccion sel;
    
    public Escenario(){
        this.vg = new Vista();
        this.llegada = new PuntoLlegada(1);
        crearHilos();

    }
    
   
    public void crearHilos(){
        double ran;
        int valorRan;
        
        ran = Math.random()*10;
        valorRan = (int)ran;
        valorRan = ((valorRan<1 || valorRan>10) ? 5 : valorRan);
        Corredor g1 = new Corredor(vg, llegada, sel);
        g1.setPriority(valorRan);
        g1.setName("Corredor 1");
//        System.out.println("prioridad Galgo1:"+g1.getPriority());       
        
        ran = Math.random()*10;
        valorRan = (int)ran;
        valorRan = ((valorRan<1 || valorRan>10) ? 5 : valorRan);
        Corredor g2 = new Corredor(vg, llegada, sel);
        g2.setPriority(valorRan);
        g2.setName("Corredor 2");
//        System.out.println("prioridad Galgo2:"+g2.getPriority());     
              
        g1.start();
        g2.start();
        
    }
       
}