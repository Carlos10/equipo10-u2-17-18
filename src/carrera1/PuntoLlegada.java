package carrera1;

public class PuntoLlegada {
    public int llegada;

    public PuntoLlegada() {
    }

    public PuntoLlegada(int llegada) {
        this.llegada = llegada;
    }

    public int getLlegada() {
        return llegada;
    }

    public void setLlegada(int llegada) {
        this.llegada = llegada;
    }

}